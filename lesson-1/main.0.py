import numpy as np
import cv2 as cv
import appendPlates

plate_cascade = cv.CascadeClassifier('../cascades/haarcascade_russian_plate_number.xml')

def detect_plate(image_filename):
    image = cv.imread(image_filename)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    plates = plate_cascade.detectMultiScale(gray, 1.1, 2)
    
    if len(plates) > 0:
        appendPlates.cutPlates(gray, plates, 2)


    
detect_plate('../images/lesson-1-raw/res_4.jpg')