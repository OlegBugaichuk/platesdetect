import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os
import math

plate_cascade = cv.CascadeClassifier('../cascades/haarcascade_russian_plate_number.xml')
images_dir = '../images/lesson-1-raw/'
save_dir = '../images/lesson-1-result/'
result_image_size = (256, 64)

def detect_plate(image_filename):
    image = cv.imread(image_filename)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    plates = plate_cascade.detectMultiScale(gray, 1.2, 5)
    result = []
    for (x,y,w,h) in plates:
        fragment = gray[y:y+h,x:x+w]
        fragment = cv.resize(fragment, result_image_size)
        result.append(fragment)
    return result

files = os.listdir(images_dir)
result_images = []
for file in files:
    res = detect_plate('{}{}'.format(images_dir, file))
    result_images = result_images + res

num_cols = 3
num_rows = math.ceil( len(result_images) / num_cols )

_,ax = plt.subplots(num_rows, num_cols)

os.makedirs(save_dir, exist_ok=True)
for i in range(num_rows):
    for j in range(num_cols):
        index = (i * num_cols) + j
        if index  < len(result_images):
            ax[i,j].imshow(result_images[index], cmap='gray')
            cv.imwrite('{}res_{}.jpg'.format(save_dir, index), result_images[index])

plt.show()