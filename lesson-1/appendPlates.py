import cv2 as cv
import numpy as np
import os

# import pytesseract

# pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"

rect_side_min = 12
rect_side_max = 60
shape_size = 64
images_dir = '../images/lesson-1-raw/'

def cutPlates(gray, rects, countPlates):
    result_image_size = (256, 64)
    result = []
    
    for (x,y,w,h) in rects:
        fragment = gray[y:y+h,x:x+w]
        fragment = cv.resize(fragment, result_image_size)
        result.append(fragment)
       
    for i in range(len(result)):
        cutSymbols(result[i], countPlates, i)
        print("Detect sucsessfully")


def cutSymbols(Plate, countPlates, count):
   
    thr_image = threshold_image(Plate)
    symbols = split_contours(thr_image)
    
    
    text = ""
    
    for i in range(len(symbols)):
        config = ('-l eng --oem 1 --psm 10 ')
    # text = text + pytesseract.image_to_string(symbols[i], lang = 'RoadNumbers')
        # text = text + pytesseract.image_to_string(symbols[i], config=config)
        
        result_dir = '{}{}'.format(images_dir, countPlates, count)
        os.makedirs(result_dir, exist_ok=True)
        
        cv.imwrite('{}/smbl{}.jpg'.format(result_dir, i), symbols[i])
        
    print(text)
     





def threshold_image(image):
    #image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    image = cv.adaptiveThreshold(image,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,35,1)
    return image


def find_contours(image):
    _, contours, _ = cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    contours = sort_contours(contours)
    print(len(contours))
    contours_poly = [None]*len(contours)
    rects = []
    for i, contour in enumerate(contours):
        contours_poly[i] = cv.approxPolyDP(contour, 3, True)
        (x,y,w,h) = cv.boundingRect(contours_poly[i])
        if (w > rect_side_min and  w < rect_side_max) and (h > rect_side_min and h < rect_side_max):
            rects.append((x,y,w,h))
    
    return rects

def split_contours(image):
    rects = find_contours(image)
    result = []
    for (x,y,w,h) in rects:
        new_image = np.zeros((shape_size,shape_size))
        new_image[:] = 255
        x_coord = int(shape_size/2 - w/2)
        y_coord  = int(shape_size/2 - h/2)
        new_image[y_coord:y_coord+h,x_coord:x_coord+w] = image[y:y+h,x:x+w]
        result.append(new_image)    
    return result

def sort_contours(contours):
    sorted_ctrs = sorted(contours, key=lambda ctr: cv.boundingRect(ctr)[0])
    return sorted_ctrs