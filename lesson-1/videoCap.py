import numpy as np
import cv2 as cv
from multiprocessing import Process, freeze_support
import threading


plate_cascade = cv.CascadeClassifier('../cascades/haarcascade_russian_plate_number.xml')

#второй поток,принимает кадр из первого потока, ищет номер и записывает если найдет в файл
def detect_plate(frame, ret):
    
    #print("ok")
    if ret:
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        plates = plate_cascade.detectMultiScale(gray, 1.1, 10)
        
        if (plates != '()'):
            for (x,y,w,h) in plates:
                cv.rectangle(frame, (x,y), (x+w, y+h), (255,0,0), 2)

            cv.imwrite('{}res_{}.jpg'.format("../images/lesson-1-raw/", 1), frame)

#первый поток, который принимает видеопоток по ссылке и передает кадры во второй поток , а также выводит поток на экран   
def videoDetect():
        
    #cap = cv.VideoCapture()
    #cap.open('rtsp://178.163.50.99:554/live/main')
    cap = cv.VideoCapture('../video/testVideo.mp4')

    while(True):
        #получает кадры с камеры
        ret, frame = cap.read()
        #запуск второго потока с параметрами
        if __name__ == '__main__':
            freeze_support()
            t = threading.Thread(target=detect_plate, args=(frame, ret))
            t.daemon = True
            t.start()
            
            
        #Отображает кадры с камеры
        cv.imshow('frame', frame)
                 
        if cv.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv.destroyAllWindows()

videoDetect()



