import cv2 as cv
import appendPlates
 
scale_factor = 1.1
min_neighbors = 4
min_size = (50, 50)
webcam=True #if working with video file then make it 'False'



def detect(path):
 
    cascade = cv.CascadeClassifier(path)
    cap = cv.VideoCapture('../video/testvideo2.mp4')
    #cap = cv.VideoCapture('rtsp://admin:recon2018@178.163.50.99:8083/Streaming/Channels/401')
    #cap.open('')
    countPic = 0
    while True:
        
        # Capture frame-by-frame
        ret, img = cap.read()
            
        #converting to gray image for faster video processing
        try:
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        except:
            continue
        rects = cascade.detectMultiScale(gray, scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size)
        
        # if at least 1 face detected
        if len(rects) > 0:
            # Draw a rectangle around the faces
            #Cut plates into img
            appendPlates.cutPlates(gray, rects, countPic)
            countPic = countPic + 1
        # Display the resulting frame
        #cv.imshow('Plate Detection on Video', img)
        #wait for 'c' to close the application
        if cv.waitKey(1) & 0xFF == ord('c'):
            break
        
    cap.release()
 
def main():
    cascadeFilePath="../cascades/haarcascade_russian_plate_number.xml"
    detect(cascadeFilePath)
    cv.destroyAllWindows()
 
 
if __name__ == "__main__":
    main()